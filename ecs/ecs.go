package ecs

import "fmt"

// EcsClusterStats holds information about ECS cluster
type EcsClusterStats struct {
	RegisteredInstancesCount int64
	RunningTasksCount        int64
	PendingTasksCount        int64
	ActiveServicesCount      int64
	ZeroTasksInstancesCount  int64
	ZeroCPUInstancesCount    int64
	ZeroMemoryInstancesCount int64
}

// GetEcsClusterStats - gathers statistics about ECS cluster
func GetEcsClusterStats(cluster string) (EcsClusterStats, error) {
	fmt.Println(cluster)
	stats := EcsClusterStats{}
	return stats, nil
}
