package main

import (
	"fmt"
	"os"

	color "github.com/fatih/color"
	flag "github.com/spf13/pflag"

	ecs "gitlab.com/mzdrale/ecsstats/ecs"
)

var (
	binName string
	cfgFile string
	cfgDir  string
	version string
	pid     int
)

// Config variables
var (
	aPrintVersion  bool
	aListClusters  bool
	aListInstances bool
	aStats         bool
	aCluster       string
)

func init() {

	// Use config from ~/.aws
	os.Setenv("AWS_SDK_LOAD_CONFIG", "true")

	// Usage
	flag.Usage = func() {
		fmt.Printf("Usage: \n")
		flag.PrintDefaults()
	}

	// Get arguments
	flag.BoolVarP(&aPrintVersion, "version", "V", false, "Print version")
	flag.BoolVarP(&aListClusters, "list-clusters", "", false, "Print list of ECS clusters")
	flag.BoolVarP(&aListInstances, "list-instances", "", false, "Print list of instances in ECS cluster")
	flag.BoolVarP(&aStats, "stats", "s", false, "Print ECS cluster statistics")
	flag.StringVarP(&aCluster, "cluster", "c", "", "ECS cluster name")

	flag.Parse()

}

func main() {

	// Set colors
	keyText := color.New(color.FgHiBlack).SprintFunc()
	red := color.New(color.FgRed).SprintFunc()
	redBold := color.New(color.FgRed, color.Bold).SprintFunc()
	cyan := color.New(color.FgCyan).SprintFunc()
	green := color.New(color.FgGreen).SprintFunc()
	yellow := color.New(color.FgYellow).SprintFunc()

	if aPrintVersion {
		fmt.Printf("\n%v %v\n\n", binName, version)
		fmt.Printf("URL: https://gitlab.com/mzdrale/ecs-tool\n\n")
		os.Exit(0)
	}

	if aListClusters {
		// 	// Get clusters list
		// 	clusters, err := aws.GetEcsClusters()

		// 	if err != nil {
		// 		fmt.Printf(red("\U00002717 Couldn't get list of ECS clusters: %v\n"), redBold(err))
		// 		os.Exit(0)
		// 	}

		// 	clustersInfo, err := aws.GetEcsClustersInfo(clusters)

		// 	if err != nil {
		// 		fmt.Printf(red("\U00002717 Couldn't get ECS clusters details: %v\n"), redBold(err))
		// 		os.Exit(0)
		// 	}

		// 	// If no clusters found, exit
		// 	if len(clusters) == 0 {
		// 		fmt.Println(cyan("\U00002717 No clusters found."))
		// 		os.Exit(0)
		// 	}

		// 	for _, cluster := range clustersInfo {
		// 		fmt.Printf(yellow("%s:\n"), cluster.Name)
		// 		fmt.Printf("  - %s %s\n", keyText("arn:"), green(cluster.ARN))
		// 		fmt.Printf("  - %s %s\n", keyText("status:"), green(cluster.Status))
		// 		fmt.Printf("  - %s %s\n", keyText("instances:"), green(cluster.RegisteredInstancesCount))
		// 		fmt.Printf("  - %s %s\n", keyText("tasks:"), green(cluster.RunningTasksCount))
		// 	}

		// 	fmt.Print("\n_________________________________________________\n\n")
		// 	fmt.Printf("   %s %s\n", keyText("Number of clusters:"), cyan(strconv.Itoa(len(clusters))))
		// 	fmt.Print("_________________________________________________\n\n")

	} else if aListInstances {

		if aCluster == "" {
			fmt.Printf(red("\n\U00002717 Missing --cluster parameter!\n"))
			fmt.Printf("\nUsage: \n")
			flag.PrintDefaults()
			os.Exit(0)
		}

		// // Get ECS cluster ARN
		// clusterARN, err := aws.GetEcsClusterArnByName(aCluster)

		// zeroCPUInstanceCount := 0
		// zeroMemoryInstanceCount := 0
		// zeroTasksRunningInstanceCount := 0

		// if err != nil {
		// 	fmt.Printf(red("\U00002717 Couldn't get ECS clusters ARN: %v\n"), redBold(err))
		// 	os.Exit(0)
		// }

		// // Get cluster instances
		// instances, err := aws.GetEcsClusterInstances(aCluster)

		// if err != nil {
		// 	fmt.Printf(red("\U00002717 Couldn't get list of instances in ECS cluster %s: %v\n"), aCluster, err)
		// }

		// // If no instances found, exit
		// if len(instances) == 0 {
		// 	fmt.Println(cyan("\U00002717 No instances found."))
		// 	os.Exit(0)
		// }

		// instancesInfo, err := aws.GetEcsClusterInstancesInfo(clusterARN, instances)

		// for _, instance := range instancesInfo {
		// 	fmt.Printf(yellow("%s:\n"), instance.Name)
		// 	fmt.Printf("  - %s %s\n", keyText("ec2 instance id:"), green(instance.Ec2InstanceID))
		// 	fmt.Printf("  - %s %s\n", keyText("running tasks:"), green(instance.RunningTasksCount))
		// 	fmt.Printf("  - %s %s\n", keyText("remaining cpu:"), green(instance.RemainingCPU))
		// 	fmt.Printf("  - %s %s\n", keyText("remaining memory:"), green(instance.RemainingMemory))

		// 	if instance.RemainingCPU == 0 {
		// 		zeroCPUInstanceCount++
		// 	}

		// 	if instance.RemainingMemory == 0 {
		// 		zeroMemoryInstanceCount++
		// 	}

		// 	if instance.RunningTasksCount == 0 {
		// 		zeroTasksRunningInstanceCount++
		// 	}
		// }

		// fmt.Print("\n_________________________________________________\n\n")
		// fmt.Printf("   %s %s\n", keyText("Number of instances:"), cyan(strconv.Itoa(len(instances))))
		// fmt.Printf("   %s %s\n", keyText("Number of instances with 0 tasks running:"), cyan(strconv.Itoa(zeroTasksRunningInstanceCount)))
		// fmt.Printf("   %s %s\n", keyText("Number of instances with 0 CPU:"), cyan(strconv.Itoa(zeroCPUInstanceCount)))
		// fmt.Printf("   %s %s\n", keyText("Number of instances with 0 memory:"), cyan(strconv.Itoa(zeroMemoryInstanceCount)))
		// fmt.Print("_________________________________________________\n\n")

	} else if aStats {
		if aCluster == "" {
			fmt.Printf(red("\n\U00002717 Missing --cluster parameter!\n"))
			fmt.Printf("\nUsage: \n")
			flag.PrintDefaults()
			os.Exit(0)
		}

		// Get ECS cluster stats
		stats, err := ecs.GetEcsClusterStats(aCluster)

		if err != nil {
			fmt.Printf(red("\U00002717 Couldn't get ECS clusters stats: %v\n"), redBold(err))
			os.Exit(0)
		}

		fmt.Printf("%v\n", stats)

		// // Get ECS cluster ARN
		// clusterARN, err := aws.GetEcsClusterArnByName(aCluster)

		// if err != nil {
		// 	fmt.Printf(red("\U00002717 Couldn't get ECS clusters ARN: %v\n"), redBold(err))
		// 	os.Exit(0)
		// }

		// // Get cluster instances
		// instances, err := aws.GetEcsClusterInstances(aCluster)

		// if err != nil {
		// 	fmt.Printf(red("\U00002717 Couldn't get list of instances in ECS cluster %s: %v\n"), aCluster, err)
		// }

		// fmt.Print("\n_________________________________________________\n\n")
		// fmt.Printf("   %s %s\n", keyText("Number of instances:"), cyan(strconv.Itoa(len(instances))))
		// // fmt.Printf("   %s %s\n", keyText("Number of instances with 0 tasks running:"), cyan(strconv.Itoa(zeroTasksRunningInstanceCount)))
		// // fmt.Printf("   %s %s\n", keyText("Number of instances with 0 CPU:"), cyan(strconv.Itoa(zeroCPUInstanceCount)))
		// // fmt.Printf("   %s %s\n", keyText("Number of instances with 0 memory:"), cyan(strconv.Itoa(zeroMemoryInstanceCount)))
		// fmt.Print("_________________________________________________\n\n")

	}
}
